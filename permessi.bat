@echo off
rem disabilito l'output a video

echo --------
echo Milano 2017 05 25 
echo By DTraining Srl 
echo www.dtraining.it
echo contatti@dtraining.it
echo --------
echo Genera l'elenco di cartelle con i relativi permessi
echo Vanno passati due parametri:
echo - il primo e' la drectory di cui si vogliono avere i permessi
echo - il seconodo e' /s per avere i permessi anche di tutte le sotto directory
echo L'output viene dato in un file chiamato AAAAMMGGHHmmPermessi.txt nella stessa directory in cui viene lanciato il programma
echo Viene anche creato un file temporaneo chiamata AAAAMMGGHHmmDirPerms.txt che sara' cancellato prima della fine del programma
echo --------
echo esempio per lista permessi della cartella utente public e di tutte le sottocartelle assumendo che il file permessi.bat si trovi in c:
echo c:\permessi.bat C:\Users\Public /S

Pause

rem Milano 2017 05 25
rem By DTraining Srl 
rem www.dtraining.it
rem contatti@dtraining.it
rem  Genera l'elenco di cartelle con i relativi permessi
rem vanno passati due parametri:
rem il primo è la drectory di cui si vogliono avere i permessi
rem il seconodo è /s per avere i permessi anche di tutte le sotto directory
rem l'output viene dato in un file chiamato AAAAMMGGHHmmPermessi.txt nella stessa directory in cui viene lanciato il programma
rem viene anche creato un file temporaneo chiamata AAAAMMGGHHmmDirPerms.txt che sarà cancellato prima della fine del programma

rem in questa parte creiamo un time stamp AAAAMMGGHHmm
For /f "tokens=1-4 delims=/ " %%a in ('date /t') do (set mydate=%%c%%b%%a)
For /f "tokens=1-3 delims=/:/ " %%a in ('time /t') do (set mytime=%%a%%b%%c)

rem creiamo le variabili per i nomi dei due files
set filename=%mydate%%mytime%Permessi.txt

set DirPerms=%mydate%%mytime%DirPerms.txt

rem se ho inserito il secondo parametro /s vado a sub per avere i permessi di tutte le sottodirectory
if /i "%2"=="/s" goto sub
rem se non ho inserito /s  scrivo il nome della directory nel file %filename% precedentemente creato e ne scrivo i permessi
CACLS %1 >>%filename%
rem il comando qui di seguito mi permette di avere i permessi di tutti i file presenti nella directory. Per usarlo cancellare la scritta rem all inizio della riga del comando 
rem CACLS %1\*.* >>%filename%

rem questo abbiamo terminato il programma e andiamo alla fine: ":eof"
goto eof
:sub
rem comincia la parte per avere i permessi di tutte le sottodirectory

rem crea un file temporaneo contenente l'elenco di tutte le sottocartelle
for /f "Tokens=*" %%i in ('dir %1  /AD /B /ON /S') do echo %%i>>%DirPerms%

rem leggo dal file temporaneo le sottocartellle esistenti e ne scrivo nome e permessi nel file %filename% precedentemente creato 
for /f "Tokens=*" %%i in ('type %DirPerms%') do CACLS "%%i ">>%filename%

rem cancello il file temporaneo contenente l'elenco di tutte le sottocartelle
del /q %DirPerms%
:eof
rem fine del programma esco dalla command line
exit